DROP VIEW IF EXISTS FormationsDansNotreRegion;

CREATE VIEW FormationsDansNotreRegion AS
SELECT
    f.*
FROM
    etablissement AS e,
    departement AS d,
    formation AS f
WHERE
    d.code_dep = e.code_dep
    AND e.code_etab = f.code_etab
    AND d.region IN (
        'Normandie',
        'Bourgogne-Franche-Comté',
        'Centre-Val de Loire'
    );

-- Ratio filles/garçons
SELECT
    SUM(nb_voeux_total_filles) / (
        SUM(nb_voeux_total - nb_voeux_total_filles) * 1.0
    ) AS pourcentage_total_filles,
    SUM(nb_voeux_total - nb_voeux_total_filles) / (SUM(nb_voeux_total_filles) * 1.0) AS pourcentage_total_garcons
FROM
    FormationsDansNotreRegion;

--LICENCE filles/garcons
SELECT
    specialite_fil,
    SUM(nb_voeux_total_filles) AS total_filles,
    SUM(nb_voeux_total - nb_voeux_total_filles) AS total_garcons,
    SUM(nb_voeux_total_filles) / SUM(nb_voeux_total * 1.0) * 100 AS pourcentage_filles,
    SUM(nb_voeux_total - nb_voeux_total_filles) / SUM(nb_voeux_total * 1.0) * 100 AS pourcentage_garcons
FROM
    FormationsDansNotreRegion AS f,
    vuefiliere AS v
WHERE
    f.fno = v.fno
    AND v.type_fil = 'Licence'
GROUP BY
    v.specialite_fil
ORDER BY
    SUM(nb_voeux_total) DESC;

--BUT filles/garcons
SELECT
    specialite_fil,
    SUM(nb_voeux_total_filles) AS total_filles,
    SUM(nb_voeux_total - nb_voeux_total_filles) AS total_garcons,
    SUM(nb_voeux_total_filles) / SUM(nb_voeux_total * 1.0) * 100 AS pourcentage_filles,
    SUM(nb_voeux_total - nb_voeux_total_filles) / SUM(nb_voeux_total * 1.0) * 100 AS pourcentage_garcons
FROM
    FormationsDansNotreRegion AS f,
    vuefiliere AS v
WHERE
    f.fno = v.fno
    AND v.type_fil = 'BUT'
GROUP BY
    v.specialite_fil
ORDER BY
    SUM(nb_voeux_total) DESC;

--BTS filles/garcons
SELECT
    specialite_fil,
    SUM(nb_voeux_total_filles) AS total_filles,
    SUM(nb_voeux_total - nb_voeux_total_filles) AS total_garcons,
    SUM(nb_voeux_total_filles) / SUM(nb_voeux_total * 1.0) * 100 AS pourcentage_filles,
    SUM(nb_voeux_total - nb_voeux_total_filles) / SUM(nb_voeux_total * 1.0) * 100 AS pourcentage_garcons
FROM
    FormationsDansNotreRegion AS f,
    vuefiliere AS v
WHERE
    f.fno = v.fno
    AND v.type_fil = 'BTS'
GROUP BY
    v.specialite_fil
ORDER BY
    SUM(nb_voeux_total) DESC;

-- type filiere / type precis filiere filles/garcons
SELECT
    v.type_fil,
    type_precis_fil,
    SUM(nb_voeux_total_filles) AS total_filles,
    SUM(nb_voeux_total - nb_voeux_total_filles) AS total_garcons,
    SUM(nb_voeux_total_filles) / SUM(nb_voeux_total * 1.0) * 100 AS pourcentage_filles,
    SUM(nb_voeux_total - nb_voeux_total_filles) / SUM(nb_voeux_total * 1.0) * 100 AS pourcentage_garcons
FROM
    FormationsDansNotreRegion AS f,
    vuefiliere AS v
WHERE
    f.fno = v.fno
    AND nb_voeux_total != 0
GROUP BY
    v.type_precis_fil,
    v.type_fil
ORDER BY
    SUM(nb_voeux_total) DESC;

SELECT
    v.type_precis_fil,
    SUM(nb_voeux_total_filles) AS total_filles,
    SUM(nb_voeux_total_filles) / (
        SELECT
            sum(nb_voeux_total_filles) * 1.0
        FROM
            FormationsDansNotreRegion AS f
    ) * 100 AS pourcentage_filles
FROM
    FormationsDansNotreRegion AS f,
    vuefiliere AS v
WHERE
    f.fno = v.fno
    AND nb_voeux_total != 0
GROUP BY
    v.type_precis_fil
ORDER BY
    SUM(nb_voeux_total_filles) DESC;

-- type fil filles
SELECT
    v.type_fil,
    SUM(nb_voeux_total_filles) AS total_filles,
    SUM(nb_voeux_total_filles) / (
        SELECT
            sum(nb_voeux_total_filles) * 1.0
        FROM
            FormationsDansNotreRegion AS f
    ) * 100 AS pourcentage_filles
FROM
    FormationsDansNotreRegion AS f,
    vuefiliere AS v
WHERE
    f.fno = v.fno
    AND nb_voeux_total != 0
GROUP BY
    v.type_fil
ORDER BY
    SUM(nb_voeux_total_filles) DESC;

-- type fil filles garcons licence
SELECT
    type_precis_fil,
    SUM(nb_voeux_total_filles) AS total_filles,
    SUM(nb_voeux_total - nb_voeux_total_filles) AS total_garcons,
    SUM(nb_voeux_total_filles) / SUM(nb_voeux_total * 1.0) * 100 AS pourcentage_filles,
    SUM(nb_voeux_total - nb_voeux_total_filles) / SUM(nb_voeux_total * 1.0) * 100 AS pourcentage_garcons
FROM
    formationsdansnotreregion AS f,
    vuefiliere AS v
WHERE
    f.fno = v.fno
    AND nb_voeux_total != 0
GROUP BY
    v.type_precis_fil
ORDER BY
    SUM(nb_voeux_total) DESC;