from utils import *


def get_create_table_instruction(columns, columns_types, use_numbers=False, table_name: str = "import") -> str:
    range_n = range(1, len(columns) + 1)
    if (use_numbers):
        final_columns = [f"n{column} {column_type}" for column,
                         column_type in zip(range_n, columns_types)]
    else:
        final_columns = [f"{column} {column_type}" for column,
                         column_type in zip(columns, columns_types)]

    return "CREATE TABLE " + table_name + " (" + ", ".join(final_columns) + ");"


if __name__ == "__main__":
    columns = get_usable_columns()
    columns_types = get_columns_types()
    print(get_create_table_instruction(columns, columns_types))
    print(get_create_table_instruction(columns, columns_types, True))
