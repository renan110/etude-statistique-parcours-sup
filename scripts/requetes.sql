-- Q1
-- n55 = (n56+n57+n58)
SELECT
    n55,
    (n56 + n57 + n58)
FROM
    import;

-- Q2
-- Renvoie 0
SELECT
    COUNT(*)
FROM
    import
WHERE
    (n56 + n57 + n58) != n55;

-- Q3
-- n73 = n50 / n46 * 100
SELECT
    n73,
    ROUND(
        CAST((n50 / n46 * 100) AS NUMERIC),
        2
    )
FROM
    import
WHERE
    n46 > 0;

-- Q4
-- Renvoie 0
SELECT
    COUNT(*)
FROM
    import
WHERE
    n46 > 0
    AND n73 != ROUND(
        CAST((n50 / n46 * 100) AS NUMERIC),
        2
    );

-- Q5
-- n76 ~= n47 / n46 * 100
-- Les données sont exactes seulement sur la partie entière
SELECT
    n76,
    CAST(n47 AS FLOAT) / n46 * 100
FROM
    import
WHERE
    n46 > 0;

-- Q6
SELECT
    CAST(nb_admissions_total_filles AS FLOAT) / nb_admissions_total * 100
FROM
    formation
WHERE
    nb_admissions_total > 0;

-- Q7
-- n81 ~= n55 / n46 * 100
-- Seule la partie entière est exacte
SELECT
    n81,
    (CAST(n55 AS FLOAT) / n46) * 100
FROM
    import
WHERE
    n46 > 0;

-- Q8
SELECT
    CAST(
        (
            nb_admissions_bac_general + nb_admissions_bac_techno + nb_admissions_bac_pro
        ) AS FLOAT
    ) / nb_admissions_total * 100
FROM
    formation
WHERE
    nb_admissions_total > 0;