DROP VIEW IF EXISTS VueFiliere;

DROP TABLE IF EXISTS Import,
Formation,
TypeFiliere,
TypePrecisFiliere,
SpecialiteFiliere,
Filiere,
Etablissement,
Departement;

CREATE TABLE import (
    n1 INT,
    n2 VARCHAR(40),
    n3 CHAR(8),
    n4 VARCHAR(140),
    n5 CHAR(3),
    n6 VARCHAR(30),
    n7 VARCHAR(30),
    n8 VARCHAR(30),
    n9 VARCHAR(30),
    n10 VARCHAR(25),
    n11 VARCHAR(20),
    n12 TEXT,
    n13 VARCHAR(80),
    n14 VARCHAR(140),
    n15 TEXT,
    n16 POINT,
    n17 INT,
    n18 INT,
    n19 INT,
    n20 INT,
    n21 INT,
    n22 INT,
    n23 INT,
    n24 INT,
    n25 INT,
    n26 INT,
    n27 INT,
    n28 INT,
    n29 INT,
    n30 INT,
    n31 INT,
    n32 INT,
    n33 INT,
    n34 INT,
    n35 INT,
    n36 INT,
    n37 INT,
    n38 INT,
    n39 INT,
    n40 INT,
    n41 INT,
    n42 INT,
    n43 INT,
    n44 INT,
    n45 INT,
    n46 INT,
    n47 INT,
    n48 INT,
    n49 INT,
    n50 FLOAT,
    n51 FLOAT,
    n52 FLOAT,
    n53 INT,
    n54 INT,
    n55 INT,
    n56 INT,
    n57 INT,
    n58 INT,
    n59 INT,
    n60 INT,
    n61 INT,
    n62 INT,
    n63 INT,
    n64 INT,
    n65 FLOAT,
    n66 INT,
    n67 INT,
    n68 INT,
    n69 INT,
    n70 INT,
    n71 INT,
    n72 INT,
    n73 FLOAT,
    n74 FLOAT,
    n75 FLOAT,
    n76 FLOAT,
    n77 FLOAT,
    n78 FLOAT,
    n79 FLOAT,
    n80 FLOAT,
    n81 FLOAT,
    n82 FLOAT,
    n83 FLOAT,
    n84 FLOAT,
    n85 FLOAT,
    n86 FLOAT,
    n87 FLOAT,
    n88 FLOAT,
    n89 FLOAT,
    n90 FLOAT,
    n91 FLOAT,
    n92 FLOAT,
    n93 FLOAT,
    n94 FLOAT,
    n95 FLOAT,
    n96 FLOAT,
    n97 FLOAT,
    n98 FLOAT,
    n99 FLOAT,
    n100 FLOAT,
    n101 VARCHAR(70),
    n102 FLOAT,
    n103 VARCHAR(40),
    n104 FLOAT,
    n105 VARCHAR(40),
    n106 FLOAT,
    n107 VARCHAR(50),
    n108 FLOAT,
    n109 VARCHAR(20),
    n110 INT,
    n111 TEXT,
    n112 TEXT,
    n113 TEXT,
    n114 FLOAT,
    n115 FLOAT,
    n116 FLOAT,
    n117 CHAR(5),
    n118 CHAR(5)
);

-- Impossible d'utiliser la commande sql COPY sur notre installation locale (sur Windows), nous utilisons donc la meta-commande Postgresql suivante pour importer le CSV
-- \copy import FROM 'data/data.csv' CSV DELIMITER ';' ENCODING 'UTF8';
-- Voici tout de même la commande sql COPY pour importer les données (ajoutée avant le rendu)
COPY import
FROM
    '<inserer_chemin>' CSV DELIMITER ';' ENCODING 'UTF8';

-- CREATION TABLES
CREATE TABLE TypeFiliere (
    tfno INT GENERATED ALWAYS AS IDENTITY,
    type_fil VARCHAR(20),
    CONSTRAINT pk_tfno PRIMARY KEY (tfno)
);

CREATE TABLE TypePrecisFiliere (
    ifno INT GENERATED ALWAYS AS IDENTITY,
    type_precis_fil VARCHAR(80),
    CONSTRAINT type_precis_filiere_pk PRIMARY KEY (ifno)
);

CREATE TABLE SpecialiteFiliere (
    sfno INT GENERATED ALWAYS AS IDENTITY,
    specialite_fil VARCHAR(140),
    CONSTRAINT pk_specialite_filiere PRIMARY KEY (sfno)
);

-- LOOKUP TABLE
CREATE TABLE Filiere (
    fno INT GENERATED ALWAYS AS IDENTITY,
    tfno INT,
    ifno INT,
    sfno INT,
    CONSTRAINT pk_filiere PRIMARY KEY(fno),
    CONSTRAINT fk_type_filiere FOREIGN KEY(tfno) REFERENCES TypeFiliere(tfno),
    CONSTRAINT fk_type_precis_filiere FOREIGN KEY(ifno) REFERENCES TypePrecisFiliere(ifno),
    CONSTRAINT fk_specialite_filiere FOREIGN KEY(sfno) REFERENCES SpecialiteFiliere(sfno)
);

CREATE VIEW VueFiliere AS
SELECT
    fno,
    TypeFiliere.*,
    TypePrecisFiliere.*,
    SpecialiteFiliere.*
FROM
    Filiere,
    TypeFiliere,
    TypePrecisFiliere,
    SpecialiteFiliere
WHERE
    Filiere.tfno = TypeFiliere.tfno
    AND Filiere.ifno = TypePrecisFiliere.ifno
    AND Filiere.sfno = SpecialiteFiliere.sfno;

CREATE TABLE Departement (
    code_dep CHAR(3),
    nom_dep VARCHAR(30),
    region VARCHAR(30),
    CONSTRAINT pk_departement PRIMARY KEY (code_dep)
);

CREATE TABLE Etablissement(
    code_etab CHAR(8),
    code_dep CHAR(3),
    nom_etab VARCHAR(140),
    statut VARCHAR(40),
    CONSTRAINT pk_etablissement PRIMARY KEY(code_etab),
    CONSTRAINT fk_departement FOREIGN KEY (code_dep) REFERENCES Departement (code_dep)
);

CREATE TABLE Formation(
    code_forma INT,
    code_etab CHAR(8),
    fno INT,
    est_selective BOOLEAN,
    capacite INT,
    nb_voeux_total INT,
    nb_voeux_total_filles INT,
    nb_voeux_pp INT,
    nb_voeux_pp_bac_general INT,
    nb_voeux_pp_bac_general_boursier INT,
    nb_voeux_pp_bac_techno INT,
    nb_voeux_pp_bac_techno_boursier INT,
    nb_voeux_pp_bac_pro INT,
    nb_voeux_pp_bac_pro_boursier INT,
    nb_voeux_pc INT,
    nb_voeux_pc_bac_general INT,
    nb_voeux_pc_bac_techno INT,
    nb_voeux_pc_bac_pro INT,
    nb_propositions_total INT,
    nb_propositions_bac_general INT,
    nb_propositions_bac_general_boursier INT,
    nb_propositions_bac_techno INT,
    nb_propositions_bac_techno_boursier INT,
    nb_propositions_bac_pro INT,
    nb_propositions_bac_pro_boursier INT,
    nb_admissions_total INT,
    nb_admissions_total_filles INT,
    nb_admissions_pp INT,
    nb_admissions_pc INT,
    nb_admissions_boursiers INT,
    nb_admissions_bac_general INT,
    nb_admissions_bac_techno INT,
    nb_admissions_bac_pro INT,
    nb_admissions_autres INT,
    nb_admissions_mention_inconnu INT,
    nb_admissions_mention_sans INT,
    nb_admissions_mention_ab INT,
    nb_admissions_mention_b INT,
    nb_admissions_mention_tb INT,
    nb_admissions_mention_tbf INT,
    nb_admissions_bac_general_mention INT,
    nb_admissions_bac_techno_mention INT,
    nb_admissions_bac_pro_mention INT,
    nb_admissions_debut_pp INT,
    nb_admissions_avant_bac INT,
    nb_admissions_avant_fin_pp INT,
    CONSTRAINT fk_etablissement FOREIGN KEY (code_etab) REFERENCES Etablissement(code_etab),
    CONSTRAINT fk_filiere FOREIGN KEY (fno) REFERENCES Filiere(fno),
    CONSTRAINT pk_formation PRIMARY KEY (code_forma)
);

-- INSERTIONS
INSERT INTO
    TypeFiliere (type_fil)
SELECT
    DISTINCT n11
FROM
    import;

INSERT INTO
    TypePrecisFiliere (type_precis_fil)
SELECT
    DISTINCT n13
FROM
    import;

INSERT INTO
    SpecialiteFiliere (specialite_fil)
SELECT
    DISTINCT n14
FROm
    import;

INSERT INTO
    Filiere (tfno, ifno, sfno)
SELECT
    DISTINCT tfno,
    ifno,
    sfno
FROM
    import,
    TypeFiliere,
    TypePrecisFiliere,
    SpecialiteFiliere
WHERE
    n11 = type_fil
    AND n13 = type_precis_fil
    AND n14 = specialite_fil;

INSERT INTO
    Departement
SELECT
    DISTINCT n5,
    n6,
    n7
FROM
    import;

INSERT INTO
    Etablissement
SELECT
    DISTINCT n3,
    n5,
    n4,
    n2
FROM
    import;

INSERT INTO
    Formation
SELECT
    n110,
    n3,
    fno,
    CASE
        WHEN n10 = 'formation sélective' THEN true
        ELSE false
    END,
    n17,
    n18,
    n19,
    n20,
    n22,
    n23,
    n24,
    n25,
    n26,
    n27,
    n29,
    n30,
    n31,
    n32,
    n45,
    n94,
    n95,
    n96,
    n97,
    n98,
    n99,
    n46,
    n47,
    n48,
    n49,
    n54,
    n56,
    n57,
    n58,
    n59,
    n60,
    n61,
    n62,
    n63,
    n64,
    n65,
    n66,
    n67,
    n68,
    n50,
    n51,
    n52
FROM
    import
    JOIN VueFiliere ON (n11, n13, n14) = (type_fil, type_precis_fil, specialite_fil);