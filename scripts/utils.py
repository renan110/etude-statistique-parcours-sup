import csv
import json

USABLE_HEADER_FILE = r"data/usable_header.txt"
READABLE_HEADER_FILE = r"data/readable_header.txt"
COLUMNS_TYPES_FILE = r"data/columns_types.txt"


def get_usable_columns():
    return get_columns(USABLE_HEADER_FILE)


def get_readable_columns():
    return get_columns(READABLE_HEADER_FILE)


def get_columns_types():
    return get_columns(COLUMNS_TYPES_FILE)


def get_columns(file: str):
    with open(file, "r", encoding="utf8") as f:
        return [line.rstrip() for line in f]
