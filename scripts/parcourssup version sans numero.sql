DROP VIEW IF EXISTS VueFiliere;

DROP TABLE IF EXISTS Import,
Formation,
TypeFiliere,
TypePrecisFiliere,
SpecialiteFiliere,
Filiere,
Etablissement,
Departement;

CREATE TABLE import (
    session INT,
    contrat_etab VARCHAR(40),
    cod_uai CHAR(8),
    g_ea_lib_vx VARCHAR(140),
    dep CHAR(3),
    dep_lib VARCHAR(30),
    region_etab_aff VARCHAR(30),
    acad_mies VARCHAR(30),
    ville_etab VARCHAR(30),
    select_form VARCHAR(25),
    fili VARCHAR(20),
    lib_comp_voe_ins TEXT,
    form_lib_voe_acc VARCHAR(80),
    fil_lib_voe_acc VARCHAR(140),
    detail_forma TEXT,
    g_olocalisation_des_formations POINT,
    capa_fin INT,
    voe_tot INT,
    voe_tot_f INT,
    nb_voe_pp INT,
    nb_voe_pp_internat INT,
    nb_voe_pp_bg INT,
    nb_voe_pp_bg_brs INT,
    nb_voe_pp_bt INT,
    nb_voe_pp_bt_brs INT,
    nb_voe_pp_bp INT,
    nb_voe_pp_bp_brs INT,
    nb_voe_pp_at INT,
    nb_voe_pc INT,
    nb_voe_pc_bg INT,
    nb_voe_pc_bt INT,
    nb_voe_pc_bp INT,
    nb_voe_pc_at INT,
    nb_cla_pp INT,
    nb_cla_pc INT,
    nb_cla_pp_internat INT,
    nb_cla_pp_pasinternat INT,
    nb_cla_pp_bg INT,
    nb_cla_pp_bg_brs INT,
    nb_cla_pp_bt INT,
    nb_cla_pp_bt_brs INT,
    nb_cla_pp_bp INT,
    nb_cla_pp_bp_brs INT,
    nb_cla_pp_at INT,
    prop_tot INT,
    acc_tot INT,
    acc_tot_f INT,
    acc_pp INT,
    acc_pc INT,
    acc_debutpp FLOAT,
    acc_datebac FLOAT,
    acc_finpp FLOAT,
    acc_internat INT,
    acc_brs INT,
    acc_neobac INT,
    acc_bg INT,
    acc_bt INT,
    acc_bp INT,
    acc_at INT,
    acc_mention_nonrenseignee INT,
    acc_sansmention INT,
    acc_ab INT,
    acc_b INT,
    acc_tb INT,
    acc_tbf FLOAT,
    acc_bg_mention INT,
    acc_bt_mention INT,
    acc_bp_mention INT,
    acc_term INT,
    acc_term_f INT,
    acc_aca_orig INT,
    acc_aca_orig_idf INT,
    pct_acc_debutpp FLOAT,
    pct_acc_datebac FLOAT,
    pct_acc_finpp FLOAT,
    pct_f FLOAT,
    pct_aca_orig FLOAT,
    pct_aca_orig_idf FLOAT,
    pct_etab_orig FLOAT,
    pct_bours FLOAT,
    pct_neobac FLOAT,
    pct_mention_nonrenseignee FLOAT,
    pct_sansmention FLOAT,
    pct_ab FLOAT,
    pct_b FLOAT,
    pct_tb FLOAT,
    pct_tbf FLOAT,
    pct_bg FLOAT,
    pct_bg_mention FLOAT,
    pct_bt FLOAT,
    pct_bt_mention FLOAT,
    pct_bp FLOAT,
    pct_bp_mention FLOAT,
    prop_tot_bg FLOAT,
    prop_tot_bg_brs FLOAT,
    prop_tot_bt FLOAT,
    prop_tot_bt_brs FLOAT,
    prop_tot_bp FLOAT,
    prop_tot_bp_brs FLOAT,
    prop_tot_at FLOAT,
    lib_grp1 VARCHAR(70),
    ran_grp1 FLOAT,
    lib_grp2 VARCHAR(40),
    ran_grp2 FLOAT,
    lib_grp3 VARCHAR(40),
    ran_grp3 FLOAT,
    list_com VARCHAR(50),
    taux_adm_psup FLOAT,
    tri VARCHAR(20),
    cod_aff_form INT,
    lib_for_voe_ins TEXT,
    detail_forma2 TEXT,
    lien_form_psup TEXT,
    taux_adm_psup_gen FLOAT,
    taux_adm_psup_techno FLOAT,
    taux_adm_psup_pro FLOAT,
    etablissement_id_paysage CHAR(5),
    composante_id_paysage CHAR(5)
);

-- Impossible d'utiliser la commande sql COPY sur notre installation locale (sur Windows), nous utilisons donc la meta-commande Postgresql suivante pour importer le CSV
-- \copy import FROM 'data/data.csv' CSV DELIMITER ';' ENCODING 'UTF8';
-- Voici tout de même la commande sql COPY pour importer les données (ajoutée avant le rendu)
COPY import
FROM
    '<inserer_chemin>' CSV DELIMITER ';' ENCODING 'UTF8';

-- CREATION TABLES
CREATE TABLE TypeFiliere (
    tfno INT GENERATED ALWAYS AS IDENTITY,
    type_fil VARCHAR(20),
    CONSTRAINT pk_tfno PRIMARY KEY (tfno)
);

CREATE TABLE TypePrecisFiliere (
    ifno INT GENERATED ALWAYS AS IDENTITY,
    type_precis_fil VARCHAR(80),
    CONSTRAINT type_precis_filiere_pk PRIMARY KEY (ifno)
);

CREATE TABLE SpecialiteFiliere (
    sfno INT GENERATED ALWAYS AS IDENTITY,
    specialite_fil VARCHAR(140),
    CONSTRAINT pk_specialite_filiere PRIMARY KEY (sfno)
);

-- LOOKUP TABLE
CREATE TABLE Filiere (
    fno INT GENERATED ALWAYS AS IDENTITY,
    tfno INT,
    ifno INT,
    sfno INT,
    CONSTRAINT pk_filiere PRIMARY KEY(fno),
    CONSTRAINT fk_type_filiere FOREIGN KEY(tfno) REFERENCES TypeFiliere(tfno),
    CONSTRAINT fk_type_precis_filiere FOREIGN KEY(ifno) REFERENCES TypePrecisFiliere(ifno),
    CONSTRAINT fk_specialite_filiere FOREIGN KEY(sfno) REFERENCES SpecialiteFiliere(sfno)
);

CREATE VIEW VueFiliere AS
SELECT
    fno,
    TypeFiliere.*,
    TypePrecisFiliere.*,
    SpecialiteFiliere.*
FROM
    Filiere,
    TypeFiliere,
    TypePrecisFiliere,
    SpecialiteFiliere
WHERE
    Filiere.tfno = TypeFiliere.tfno
    AND Filiere.ifno = TypePrecisFiliere.ifno
    AND Filiere.sfno = SpecialiteFiliere.sfno;

CREATE TABLE Departement (
    code_dep CHAR(3),
    nom_dep VARCHAR(30),
    region VARCHAR(30),
    CONSTRAINT pk_departement PRIMARY KEY (code_dep)
);

CREATE TABLE Etablissement(
    code_etab CHAR(8),
    code_dep CHAR(3),
    nom_etab VARCHAR(140),
    statut VARCHAR(40),
    CONSTRAINT pk_etablissement PRIMARY KEY(code_etab),
    CONSTRAINT fk_departement FOREIGN KEY (code_dep) REFERENCES Departement (code_dep)
);

CREATE TABLE Formation(
    code_forma INT,
    code_etab CHAR(8),
    fno INT,
    est_selective BOOLEAN,
    capacite INT,
    nb_voeux_total INT,
    nb_voeux_total_filles INT,
    nb_voeux_pp INT,
    nb_voeux_pp_bac_general INT,
    nb_voeux_pp_bac_general_boursier INT,
    nb_voeux_pp_bac_techno INT,
    nb_voeux_pp_bac_techno_boursier INT,
    nb_voeux_pp_bac_pro INT,
    nb_voeux_pp_bac_pro_boursier INT,
    nb_voeux_pc INT,
    nb_voeux_pc_bac_general INT,
    nb_voeux_pc_bac_techno INT,
    nb_voeux_pc_bac_pro INT,
    nb_propositions_total INT,
    nb_propositions_bac_general INT,
    nb_propositions_bac_general_boursier INT,
    nb_propositions_bac_techno INT,
    nb_propositions_bac_techno_boursier INT,
    nb_propositions_bac_pro INT,
    nb_propositions_bac_pro_boursier INT,
    nb_admissions_total INT,
    nb_admissions_total_filles INT,
    nb_admissions_pp INT,
    nb_admissions_pc INT,
    nb_admissions_boursiers INT,
    nb_admissions_bac_general INT,
    nb_admissions_bac_techno INT,
    nb_admissions_bac_pro INT,
    nb_admissions_autres INT,
    nb_admissions_mention_inconnu INT,
    nb_admissions_mention_sans INT,
    nb_admissions_mention_ab INT,
    nb_admissions_mention_b INT,
    nb_admissions_mention_tb INT,
    nb_admissions_mention_tbf INT,
    nb_admissions_bac_general_mention INT,
    nb_admissions_bac_techno_mention INT,
    nb_admissions_bac_pro_mention INT,
    nb_admissions_debut_pp INT,
    nb_admissions_avant_bac INT,
    nb_admissions_avant_fin_pp INT,
    CONSTRAINT fk_etablissement FOREIGN KEY (code_etab) REFERENCES Etablissement(code_etab),
    CONSTRAINT fk_filiere FOREIGN KEY (fno) REFERENCES Filiere(fno),
    CONSTRAINT pk_formation PRIMARY KEY (code_forma)
);

-- INSERTIONS
INSERT INTO
    TypeFiliere (type_fil)
SELECT
    DISTINCT fili
FROM
    import;

INSERT INTO
    TypePrecisFiliere (type_precis_fil)
SELECT
    DISTINCT form_lib_voe_acc
FROM
    import;

INSERT INTO
    SpecialiteFiliere (specialite_fil)
SELECT
    DISTINCT fil_lib_voe_acc
FROm
    import;

INSERT INTO
    Filiere (tfno, ifno, sfno)
SELECT
    DISTINCT tfno,
    ifno,
    sfno
FROM
    import,
    TypeFiliere,
    TypePrecisFiliere,
    SpecialiteFiliere
WHERE
    fili = type_fil
    AND form_lib_voe_acc = type_precis_fil
    AND fil_lib_voe_acc = specialite_fil;

INSERT INTO
    Departement
SELECT
    DISTINCT dep,
    dep_lib,
    region_etab_aff
FROM
    import;

INSERT INTO
    Etablissement
SELECT
    DISTINCT cod_uai,
    dep,
    g_ea_lib_vx,
    contrat_etab
FROM
    import;

INSERT INTO
    Formation
SELECT
    cod_aff_form,
    cod_uai,
    fno,
    CASE
        WHEN select_form = 'formation sélective' THEN true
        ELSE false
    END,
    capa_fin,
    voe_tot,
    voe_tot_f,
    nb_voe_pp,
    nb_voe_pp_bg,
    nb_voe_pp_bg_brs,
    nb_voe_pp_bt,
    nb_voe_pp_bt_brs,
    nb_voe_pp_bp,
    nb_voe_pp_bp_brs,
    nb_voe_pc,
    nb_voe_pc_bg,
    nb_voe_pc_bt,
    nb_voe_pc_bp,
    prop_tot,
    prop_tot_bg,
    prop_tot_bg_brs,
    prop_tot_bt,
    prop_tot_bt_brs,
    prop_tot_bp,
    prop_tot_bp_brs,
    acc_tot,
    acc_tot_f,
    acc_pp,
    acc_pc,
    acc_brs,
    acc_bg,
    acc_bt,
    acc_bp,
    acc_at,
    acc_mention_nonrenseignee,
    acc_sansmention,
    acc_ab,
    acc_b,
    acc_tb,
    acc_tbf,
    acc_bg_mention,
    acc_bt_mention,
    acc_bp_mention,
    acc_debutpp,
    acc_datebac,
    acc_finpp
FROM
    import
    JOIN VueFiliere ON (fili, form_lib_voe_acc, fil_lib_voe_acc) = (type_fil, type_precis_fil, specialite_fil);