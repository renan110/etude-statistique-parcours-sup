-- Q1
-- n55 = (n56+n57+n58)
SELECT
    acc_neobac,
    (acc_bg + acc_bt + acc_bp)
FROM
    import;

-- Q2
-- Renvoie 0
SELECT
    COUNT(*)
FROM
    import
WHERE
    (acc_bg + acc_bt + acc_bp) != acc_neobac;

-- Q3
-- n73 = n50 / n46 * 100
SELECT
    pct_acc_debutpp,
    ROUND(
        CAST((acc_debutpp / acc_tot * 100) AS NUMERIC),
        2
    )
FROM
    import
WHERE
    acc_tot > 0;

-- Q4
-- Renvoie 0
SELECT
    COUNT(*)
FROM
    import
WHERE
    acc_tot > 0
    AND pct_acc_debutpp != ROUND(
        CAST((acc_debutpp / acc_tot * 100) AS NUMERIC),
        2
    );

-- Q5
-- n76 ~= n47 / n46 * 100
-- Les données sont exactes seulement sur la partie entière
SELECT
    pct_f,
    CAST(acc_tot_f AS FLOAT) / acc_tot * 100
FROM
    import
WHERE
    acc_tot > 0;

-- Q6
SELECT
    CAST(nb_admissions_total_filles AS FLOAT) / nb_admissions_total * 100
FROM
    formation
WHERE
    nb_admissions_total > 0;

-- Q7
-- n81 ~= n55 / n46 * 100
-- Seule la partie entière est exacte
SELECT
    pct_neobac,
    (CAST(acc_neobac AS FLOAT) / acc_tot) * 100
FROM
    import
WHERE
    acc_tot > 0;

-- Q8
SELECT
    CAST(
        (
            nb_admissions_bac_general + nb_admissions_bac_techno + nb_admissions_bac_pro
        ) AS FLOAT
    ) / nb_admissions_total * 100
FROM
    formation
WHERE
    nb_admissions_total > 0;