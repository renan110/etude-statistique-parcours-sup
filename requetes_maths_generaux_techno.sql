--requetes_maths_generaux_techno

DROP VIEW IF EXISTS FormationsDansNotreRegion;

CREATE VIEW FormationsDansNotreRegion AS
SELECT
    f.*
FROM
    etablissement AS e,
    departement AS d,
    formation AS f
WHERE
    d.code_dep = e.code_dep
    AND e.code_etab = f.code_etab
    AND d.region IN (
        'Normandie',
        'Bourgogne-Franche-Comté',
        'Centre-Val de Loire'
    );


--Requetes du taux de proposition des bac généraux et technologiques par rapport aux voeux effectués pour des formations séléctives
SELECT 
est_selective,
avg(COALESCE(nb_propositions_bac_techno / NULLIF((nb_voeux_pp_bac_techno + nb_voeux_pc_bac_techno)*1.0, 0)*100.0,0)) AS moy_bac_technologique,
avg(COALESCE(nb_propositions_bac_general / NULLIF((nb_voeux_pp_bac_general + nb_voeux_pc_bac_general)*1.0, 0)*100.0,0)) AS moy_bac_general,
stddev(COALESCE(nb_propositions_bac_techno / NULLIF((nb_voeux_pp_bac_techno + nb_voeux_pc_bac_techno)*1.0, 0)*100.0,0)) AS ecart_type_techno,
stddev(COALESCE(nb_propositions_bac_general / NULLIF((nb_voeux_pp_bac_general + nb_voeux_pc_bac_general)*1.0, 0)*100.0,0))AS ecart_type_gen,
PERCENTILE_CONT(0.25) WITHIN GROUP (ORDER BY (COALESCE(nb_propositions_bac_general / NULLIF((nb_voeux_pp_bac_general + nb_voeux_pc_bac_general)*1.0, 0)*100.0,0))) as Q1_gen,
PERCENTILE_CONT(0.25) WITHIN GROUP (ORDER BY (COALESCE(nb_propositions_bac_techno / NULLIF((nb_voeux_pp_bac_techno + nb_voeux_pc_bac_techno)*1.0, 0)*100.0,0))) as Q1_tec,
PERCENTILE_CONT(0.5) WITHIN GROUP (ORDER BY (COALESCE(nb_propositions_bac_general / NULLIF((nb_voeux_pp_bac_general + nb_voeux_pc_bac_general)*1.0, 0)*100.0,0))) as med_gen,
PERCENTILE_CONT(0.5) WITHIN GROUP (ORDER BY (COALESCE(nb_propositions_bac_techno / NULLIF((nb_voeux_pp_bac_techno + nb_voeux_pc_bac_techno)*1.0, 0)*100.0,0))) as med_tec,
PERCENTILE_CONT(0.75) WITHIN GROUP (ORDER BY (COALESCE(nb_propositions_bac_general / NULLIF((nb_voeux_pp_bac_general + nb_voeux_pc_bac_general)*1.0, 0)*100.0,0))) as Q3_gen,
PERCENTILE_CONT(0.75) WITHIN GROUP (ORDER BY (COALESCE(nb_propositions_bac_techno / NULLIF((nb_voeux_pp_bac_techno + nb_voeux_pc_bac_techno)*1.0, 0)*100.0,0))) as Q3_tec
FROM formationsDansNotreRegion as f
WHERE COALESCE(nb_propositions_bac_techno / NULLIF((nb_voeux_pp_bac_techno + nb_voeux_pc_bac_techno)*1.0, 0)*100.0,0) <= 100
AND COALESCE(nb_propositions_bac_general / NULLIF((nb_voeux_pp_bac_general + nb_voeux_pc_bac_general)*1.0, 0)*100.0,0) <= 100
GROUP BY est_selective;


SELECT 
(nb_propositions_bac_techno / ((nb_voeux_pp_bac_techno + nb_voeux_pc_bac_techno)*1.0)*100.0) AS moy_bac_technologique,
(nb_propositions_bac_general / ((nb_voeux_pp_bac_general + nb_voeux_pc_bac_general)*1.0)*100.0) AS moy_bac_general
FROM formationsDansNotreRegion as f 
WHERE (nb_propositions_bac_techno / ((nb_voeux_pp_bac_techno + nb_voeux_pc_bac_techno)*1.0)*100.0) <= 100
AND (nb_propositions_bac_general / ((nb_voeux_pp_bac_general + nb_voeux_pc_bac_general)*1.0)*100.0) <= 100
AND (nb_voeux_pp_bac_techno + nb_voeux_pc_bac_techno > 10 
AND nb_voeux_pp_bac_general + nb_voeux_pc_bac_general > 10)
AND est_selective = true;


SELECT 
(nb_propositions_bac_techno / NULLIF((nb_voeux_pp_bac_techno + nb_voeux_pc_bac_techno)*1.0, 0)*100.0,0) AS moy_bac_technologique,
(nb_propositions_bac_general / NULLIF((nb_voeux_pp_bac_general + nb_voeux_pc_bac_general)*1.0, 0)*100.0,0) AS moy_bac_general
FROM formationsDansNotreRegion as f 
WHERE COALESCE(nb_propositions_bac_techno / NULLIF((nb_voeux_pp_bac_techno + nb_voeux_pc_bac_techno)*1.0, 0)*100.0,0) <= 100
AND COALESCE(nb_propositions_bac_general / NULLIF((nb_voeux_pp_bac_general + nb_voeux_pc_bac_general)*1.0, 0)*100.0,0) <= 100
AND est_selective = true;




SELECT 
COALESCE(nb_propositions_bac_techno / NULLIF((nb_voeux_pp_bac_techno + nb_voeux_pc_bac_techno)*1.0, 0)*100.0,0) AS moy_bac_technologique,
COALESCE(nb_propositions_bac_general / NULLIF((nb_voeux_pp_bac_general + nb_voeux_pc_bac_general)*1.0, 0)*100.0,0) AS moy_bac_general
FROM formationsDansNotreRegion as f 
WHERE COALESCE(nb_propositions_bac_techno / NULLIF((nb_voeux_pp_bac_techno + nb_voeux_pc_bac_techno)*1.0, 0)*100.0,0) <= 100
AND COALESCE(nb_propositions_bac_general / NULLIF((nb_voeux_pp_bac_general + nb_voeux_pc_bac_general)*1.0, 0)*100.0,0) <= 100
AND est_selective = false;


SELECT 
--avg(COALESCE(nb_propositions_bac_techno / NULLIF((nb_voeux_pp_bac_techno + nb_voeux_pc_bac_techno)*1.0, 0)*100.0,0)) AS bac_technologique,
avg(COALESCE(nb_propositions_bac_general / NULLIF((nb_voeux_pp_bac_general + nb_voeux_pc_bac_general)*1.0, 0)*100.0,0)) AS bac_general,
--100 - avg(COALESCE(nb_propositions_bac_techno / NULLIF((nb_voeux_pp_bac_techno + nb_voeux_pc_bac_techno)*1.0, 0)*100.0,0)) AS bac_technologique,
100 - avg(COALESCE(nb_propositions_bac_general / NULLIF((nb_voeux_pp_bac_general + nb_voeux_pc_bac_general)*1.0, 0)*100.0,0)) AS bac_general

FROM formationsdansnotreregion
WHERE est_selective = true
AND COALESCE(nb_propositions_bac_techno / NULLIF((nb_voeux_pp_bac_techno + nb_voeux_pc_bac_techno)*1.0, 0)*100.0,0) <= 100
AND COALESCE(nb_propositions_bac_general / NULLIF((nb_voeux_pp_bac_general + nb_voeux_pc_bac_general)*1.0, 0)*100.0,0) <= 100;



SELECT 
nb_voeux_pc_bac_general + nb_voeux_pp_bac_general as voeux_gen,
nb_propositions_bac_general,
nb_voeux_pc_bac_techno + nb_voeux_pp_bac_techno as voeux_tec,
nb_propositions_bac_techno,
(nb_propositions_bac_techno / NULLIF((nb_voeux_pp_bac_techno + nb_voeux_pc_bac_techno)*1.0, 0)*100.0,0) AS bac_technologique,
(nb_propositions_bac_general / NULLIF((nb_voeux_pp_bac_general + nb_voeux_pc_bac_general)*1.0, 0)*100.0,0) AS bac_general,
type_fil,
type_precis_fil
FROM formation as f, departement as d, etablissement as e, typefiliere as t, filiere as fil,typeprecisfiliere as tpf
WHERE est_selective = true
AND t.tfno = fil.tfno
AND tpf.ifno = fil.ifno
AND f.fno = fil.fno
AND f.code_etab = e.code_etab
AND e.code_dep = d.code_dep
AND type_fil LIKE 'Licence'
AND d.region IN (
        'Normandie',
        'Bourgogne-Franche-Comté',
        'Centre-Val de Loire')
AND COALESCE(nb_propositions_bac_techno / NULLIF((nb_voeux_pp_bac_techno + nb_voeux_pc_bac_techno)*1.0, 0)*100.0,0) <= 100
AND COALESCE(nb_propositions_bac_general / NULLIF((nb_voeux_pp_bac_general + nb_voeux_pc_bac_general)*1.0, 0)*100.0,0) <= 100;

SELECT 
nb_voeux_pc_bac_general + nb_voeux_pp_bac_general as voeux_gen,
nb_propositions_bac_general,
nb_voeux_pc_bac_techno + nb_voeux_pp_bac_techno as voeux_tec,
nb_propositions_bac_techno,
(nb_propositions_bac_techno / NULLIF((nb_voeux_pp_bac_techno + nb_voeux_pc_bac_techno)*1.0, 0)*100.0,0) AS bac_technologique,
(nb_propositions_bac_general / NULLIF((nb_voeux_pp_bac_general + nb_voeux_pc_bac_general)*1.0, 0)*100.0,0) AS bac_general,
type_fil,
type_precis_fil
FROM formation as f, departement as d, etablissement as e, typefiliere as t, filiere as fil,typeprecisfiliere as tpf
WHERE t.tfno = fil.tfno
AND tpf.ifno = fil.ifno
AND f.fno = fil.fno
AND f.code_etab = e.code_etab
AND e.code_dep = d.code_dep
AND type_fil LIKE 'Licence'
AND d.region IN (
        'Normandie',
        'Bourgogne-Franche-Comté',
        'Centre-Val de Loire')
AND COALESCE(nb_propositions_bac_techno / NULLIF((nb_voeux_pp_bac_techno + nb_voeux_pc_bac_techno)*1.0, 0)*100.0,0) <= 100
AND COALESCE(nb_propositions_bac_general / NULLIF((nb_voeux_pp_bac_general + nb_voeux_pc_bac_general)*1.0, 0)*100.0,0) <= 100;




--typefiliere Ex : licence , but, bts
select type_fil, type_precis_fil
from typeprecisfiliere as tpf, typefiliere as tf, filiere as f, formation as form, departement as d, etablissement as e
WHERE tpf.ifno = f.ifno 
AND d.code_dep = e.code_dep
AND e.code_etab = form.code_etab
AND form.fno = f.fno
and tf.tfno = f.tfno 
AND d.region IN (
        'Normandie',
        'Bourgogne-Franche-Comté',
        'Centre-Val de Loire')
and est_selective = true
and type_fil like 'Licence';

SELECT nb_voeux_pp_bac_general + nb_voeux_pc_bac_general as nb_voeux_general, 
nb_voeux_pp_bac_techno + nb_voeux_pc_bac_techno as nb_voeux_techno, 
nb_propositions_bac_general, 
nb_propositions_bac_techno,
FROM formation
WHERE est_selective = true
AND COALESCE(nb_propositions_bac_techno / NULLIF((nb_voeux_pp_bac_techno + nb_voeux_pc_bac_techno)*1.0, 0)*100.0,0) <= 100
AND COALESCE(nb_propositions_bac_general / NULLIF((nb_voeux_pp_bac_general + nb_voeux_pc_bac_general)*1.0, 0)*100.0,0) <= 100
GROUP BY ROUND(COALESCE(nb_propositions_bac_techno / NULLIF((nb_voeux_pp_bac_techno + nb_voeux_pc_bac_techno)*1.0, 0)*100.0,0), -1) AS pourcentage_techno,
COALESCE(nb_propositions_bac_general / NULLIF((nb_voeux_pp_bac_general + nb_voeux_pc_bac_general)*1.0, 0)*100.0,0) AS pourcentage_general;

SELECT * from formation;

SELECT distinct n10 from import;

SELECT *
FROM formation
WHERE code_forma = 24685;


SELECT SUM(nb_voeux_pp_bac_general + nb_voeux_pc_bac_general) as nb_voeux_general, 
SUM(nb_voeux_pp_bac_techno + nb_voeux_pc_bac_techno) as nb_voeux_techno, 
SUM(nb_propositions_bac_general) AS total_general, 
SUM(nb_propositions_bac_techno) AS total_techno,
AVG(COALESCE(nb_propositions_bac_techno / NULLIF((nb_voeux_pp_bac_techno + nb_voeux_pc_bac_techno)*1.0, 0)*100.0,0)) AS pourcentage_techno,
AVG(COALESCE(nb_propositions_bac_general / NULLIF((nb_voeux_pp_bac_general + nb_voeux_pc_bac_general)*1.0, 0)*100.0,0)) AS pourcentage_general,
CORR(nb_voeux_pp_bac_general + nb_voeux_pc_bac_general, nb_propositions_bac_general) AS coeff_correlation_general,
CORR(nb_voeux_pp_bac_techno + nb_voeux_pc_bac_techno, nb_propositions_bac_techno) AS coeff_correlation_techno,

CORR((COALESCE(nb_propositions_bac_techno / NULLIF((nb_voeux_pp_bac_techno + nb_voeux_pc_bac_techno)*1.0, 0)*100.0,0)) , 
COALESCE(nb_propositions_bac_general / NULLIF((nb_voeux_pp_bac_general + nb_voeux_pc_bac_general)*1.0, 0)*100.0,0))

FROM formation
WHERE est_selective = true
AND COALESCE(nb_propositions_bac_techno / NULLIF((nb_voeux_pp_bac_techno + nb_voeux_pc_bac_techno)*1.0, 0)*100.0,0) <= 100
AND COALESCE(nb_propositions_bac_general / NULLIF((nb_voeux_pp_bac_general + nb_voeux_pc_bac_general)*1.0, 0)*100.0,0) <= 100;
