# Work

## Etape 1: téléchargement et analyse du fichier

Le script **data_setup** s'occupe de faire ce travail

### Analyse du header

```bash
sed -n 1p data.csv | tr ';' '\n' > readable_header.txt
```

### Analyse d'une ligne

```bash
sed -n 2p data.csv | tr ';' '\n' > firstline.txt
```

### Taille des données

`wc data.csv`

**13397 lignes** dont 1 pour header  
**12744468 octets**  

`wc readable_header.txt`

**118 colonnes**

## Etape 2: import des données

### Creation base de données

```sql
CREATE DATABASE parcoursup;
```

### Import des données

Voir **import_table.py**

```sql
CREATE TABLE import (session INT, contrat_etab VARCHAR(40), cod_uai CHAR(8), g_ea_lib_vx VARCHAR(140), dep CHAR(3), dep_lib VARCHAR(30), region_etab_aff VARCHAR(30), acad_mies VARCHAR(30), ville_etab VARCHAR(30), select_form VARCHAR(25), fili VARCHAR(20), lib_comp_voe_ins TEXT, form_lib_voe_acc VARCHAR(80), fil_lib_voe_acc VARCHAR(140), detail_forma TEXT, g_olocalisation_des_formations POINT, capa_fin INT, voe_tot INT, voe_tot_f INT, nb_voe_pp INT, nb_voe_pp_internat INT, nb_voe_pp_bg INT, nb_voe_pp_bg_brs INT, nb_voe_pp_bt INT, nb_voe_pp_bt_brs INT, nb_voe_pp_bp INT, nb_voe_pp_bp_brs INT, nb_voe_pp_at INT, nb_voe_pc INT, nb_voe_pc_bg INT, nb_voe_pc_bt INT, nb_voe_pc_bp INT, nb_voe_pc_at INT, nb_cla_pp INT, nb_cla_pc INT, nb_cla_pp_internat INT, nb_cla_pp_pasinternat INT, nb_cla_pp_bg INT, nb_cla_pp_bg_brs INT, nb_cla_pp_bt INT, nb_cla_pp_bt_brs INT, nb_cla_pp_bp INT, nb_cla_pp_bp_brs INT, nb_cla_pp_at INT, prop_tot INT, acc_tot INT, acc_tot_f INT, acc_pp INT, acc_pc INT, acc_debutpp FLOAT, acc_datebac FLOAT, acc_finpp FLOAT, acc_internat INT, acc_brs INT, acc_neobac INT, acc_bg INT, acc_bt INT, acc_bp INT, acc_at INT, acc_mention_nonrenseignee INT, acc_sansmention INT, acc_ab INT, acc_b INT, acc_tb INT, acc_tbf FLOAT, acc_bg_mention INT, acc_bt_mention INT, acc_bp_mention INT, acc_term INT, acc_term_f INT, acc_aca_orig INT, acc_aca_orig_idf INT, pct_acc_debutpp FLOAT, pct_acc_datebac FLOAT, pct_acc_finpp FLOAT, pct_f FLOAT, pct_aca_orig FLOAT, pct_aca_orig_idf FLOAT, pct_etab_orig FLOAT, pct_bours FLOAT, pct_neobac FLOAT, pct_mention_nonrenseignee FLOAT, pct_sansmention FLOAT, pct_ab FLOAT, pct_b FLOAT, pct_tb FLOAT, pct_tbf FLOAT, pct_bg FLOAT, pct_bg_mention FLOAT, pct_bt FLOAT, pct_bt_mention FLOAT, pct_bp FLOAT, pct_bp_mention FLOAT, prop_tot_bg FLOAT, prop_tot_bg_brs FLOAT, prop_tot_bt FLOAT, prop_tot_bt_brs FLOAT, prop_tot_bp FLOAT, prop_tot_bp_brs FLOAT, prop_tot_at FLOAT, lib_grp1 VARCHAR(70), ran_grp1 FLOAT, lib_grp2 VARCHAR(40), ran_grp2 FLOAT, lib_grp3 VARCHAR(40), ran_grp3 FLOAT, list_com VARCHAR(50), taux_adm_psup FLOAT, tri VARCHAR(20), cod_aff_form INT, lib_for_voe_ins TEXT, detail_forma2 TEXT, lien_form_psup TEXT, taux_adm_psup_gen FLOAT, taux_adm_psup_techno FLOAT, taux_adm_psup_pro FLOAT, etablissement_id_paysage CHAR(5), composante_id_paysage CHAR(5));
```

OU

```sql
CREATE TABLE import (n1 INT, n2 VARCHAR(40), n3 CHAR(8), n4 VARCHAR(140), n5 CHAR(3), n6 VARCHAR(30), n7 VARCHAR(30), n8 VARCHAR(30), n9 VARCHAR(30), n10 VARCHAR(25), n11 VARCHAR(20), n12 TEXT, n13 VARCHAR(80), n14 VARCHAR(140), n15 TEXT, n16 POINT, n17 INT, n18 INT, n19 INT, n20 INT, n21 INT, n22 INT, n23 INT, n24 INT, n25 INT, n26 INT, n27 INT, n28 INT, n29 INT, n30 INT, n31 INT, n32 INT, n33 INT, n34 INT, n35 INT, n36 INT, n37 INT, n38 INT, n39 INT, n40 INT, n41 INT, n42 INT, n43 INT, n44 INT, n45 INT, n46 INT, n47 INT, n48 INT, n49 INT, n50 FLOAT, n51 FLOAT, n52 FLOAT, n53 INT, n54 INT, n55 INT, n56 INT, n57 INT, n58 INT, n59 INT, n60 INT, n61 INT, n62 INT, n63 INT, n64 INT, n65 FLOAT, n66 INT, n67 INT, n68 INT, n69 INT, 
n70 INT, n71 INT, n72 INT, n73 FLOAT, n74 FLOAT, n75 FLOAT, n76 FLOAT, n77 FLOAT, n78 FLOAT, n79 FLOAT, n80 FLOAT, n81 FLOAT, n82 FLOAT, n83 FLOAT, n84 FLOAT, n85 FLOAT, n86 FLOAT, n87 FLOAT, n88 FLOAT, n89 FLOAT, n90 FLOAT, n91 FLOAT, n92 FLOAT, n93 FLOAT, n94 FLOAT, n95 FLOAT, n96 FLOAT, n97 FLOAT, n98 FLOAT, n99 FLOAT, n100 FLOAT, n101 VARCHAR(70), n102 FLOAT, n103 VARCHAR(40), n104 FLOAT, n105 VARCHAR(40), n106 FLOAT, n107 VARCHAR(50), n108 FLOAT, n109 VARCHAR(20), n110 INT, n111 TEXT, n112 TEXT, n113 TEXT, n114 FLOAT, n115 FLOAT, n116 FLOAT, n117 CHAR(5), n118 CHAR(5));
```

Puis

```sql
\copy import FROM 'data/data.csv' CSV DELIMITER ';' ENCODING 'UTF8';
```

### Analyse et interprétation des données

Le fichier **data analyse.md** contient l'analyse des données, comment elles seront éventuellement assemblées etc.
