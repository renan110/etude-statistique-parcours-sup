# Data analyse

## Établissement

Relié à chaque **formation**

- code_uai => cod_uai (*primary*)
- nom => g_ea_lib_vx
- statut => contrat_etab
- ville => ville_etab

## Département

Relié à chaque **ville**

- code => dep (*primary*)
- nom => dep_lib
- région => region_etab_aff

## Formation

- code => cod_aff_form (*primary*)
- est_sélective => select_form
- capacité => capa_fin

## Filière

Relié à chaque **formation**

- fno (*numéro auto*)
- type_fil => fili
- intitulé => form_lib_voe_acc
- spécialité => fil_lib_voe_acc

## StatsFormation

Séparation par thème, toutes ces données peuvent en théorie être mises dans la table formation

### Effectifs totaux

- **voe_tot** => Effectif total des candidats pour une formation
- **voe_tot_f** => Dont effectif des candidates pour une formation

Note: voe_tot = (nb_voe_pp +nb_voe_pc)
Note: voe_tot = (nb_voe_pc_bg + nb_voe_pc_bt + nb_voe_pc_bp +nb_voe_pc_at + nb_voe_pp_bg +nb_voe_pp_bt + nb_voe_pp_bp +nb_voe_pp_at)

### Effectifs des voeux en phase principale

- **nb_voe_pp** => Effectif total des candidats en phase principale
- **nb_voe_pp_internat** => Dont effectif des candidats ayant postulé en internat

- **nb_voe_pp_bg** => Effectif des candidats néo bacheliers généraux en phase principale
- **nb_voe_pp_bg_brs** => Dont effectif des candidats boursiers néo bacheliers généraux en phase principale

- **nb_voe_pp_bt** => Effectif des candidats néo bacheliers technologiques en phase principale
- **nb_voe_pp_bt_brs** => Dont effectif des candidats boursiers néo bacheliers technologiques en phase principale

- **nb_voe_pp_bp** => Effectif des candidats néo bacheliers professionnels en phase principale
- **nb_voe_pp_bp_brs** => Dont effectif des candidats boursiers néo bacheliers professionnels en phase principale

- (ne pas garder car **nb_voe_pp**) **nb_voe_pp_at** => Effectif des autres candidats en phase principale

**nb_voe_pp** = (**nb_voe_pp_bg** + **nb_voe_pp_bt** + **nb_voe_pp_bp** + **nb_voe_pp_at**)

### Effectifs des voeux en phase complémentaire

- **nb_voe_pc** => Effectif total des candidats en phase complémentaire

- **nb_voe_pc_bg** => Effectif des candidats néo bacheliers généraux en phase complémentaire
- **nb_voe_pc_bt** => Effectif des candidats néo bacheliers technologique en phase complémentaire
- **nb_voe_pc_bp** => Effectif des candidats néo bacheliers professionnels en phase complémentaire

- (ne pas garder car **nb_voe_pc**) **nb_voe_pc_at** => Effectifs des autres candidats en phase complémentaire

**nb_voe_pc** = (**nb_voe_pc_bg** + **nb_voe_pc_bt** + **nb_voe_pc_bp** + **nb_voe_pc_at**)

### Effectifs classements

- **nb_cla_pp** => Effectif total des candidats classés par l’établissement en phase principale
- **nb_cla_pc** => Effectif des candidats classés par l’établissement en phase complémentaire

- **nb_cla_pp_internat** => Effectif des candidats classés par l’établissement en internat (CPGE)
- **nb_cla_pp_pasinternat** => Effectif des candidats classés par l’établissement hors internat (CPGE)

- **nb_cla_pp_bg** => Effectif des candidats néo bacheliers généraux classés par l’établissement
- **nb_cla_pp_bg_brs** => Dont effectif des candidats boursiers néo bacheliers généraux classés par l’établissement
  
- **nb_cla_pp_bt** => Effectif des candidats néo bacheliers technologiques classés par l’établissement
- **nb_cla_pp_bt_brs** => Dont effectif des candidats boursiers néo bacheliers technologiques classés par l’établissement

- **nb_cla_pp_bp** => Effectif des candidats néo bacheliers professionnels classés par l’établissement
- **nb_cla_pp_bp_brs** => Dont effectif des candidats boursiers néo bacheliers professionnels classés par l’établissement

- (ne pas garder car **nb_cla_pp**) **nb_cla_pp_at** => Effectif des autres candidats classés par l’établissement

### Effectifs propositions d'admission

- **prop_tot** => Effectif total des candidats ayant reçu une proposition d’admission de la part de l’établissement
  
- **prop_tot_bg** => Effectif des candidats en terminale générale ayant reçu une proposition d’admission de la part de l’établissement
- **prop_tot_bg_brs** => Dont effectif des candidats boursiers en terminale générale ayant reçu une proposition d’admission de la part de l’établissement
  
- **prop_tot_bt** => Effectif des candidats en terminale technologique ayant reçu une proposition d’admission de la part de l’établissement
- **prop_tot_bt_brs** => Dont effectif des candidats boursiers en terminale technologique ayant reçu une proposition d’admission de la part de l’établissement

- **prop_tot_bp** => Effectif des candidats en terminale professionnelle ayant reçu une proposition d’admission de la part de l’établissement
- **prop_tot_bp_brs** => Dont effectif des candidats boursiers en terminale générale professionnelle ayant reçu une proposition d’admission de la part de l’établissement

- **prop_tot_at** => Effectif des autres candidats ayant reçu une proposition d’admission de la part de l’établissement

### Effectifs admis

- **acc_tot** => Effectif total des candidats ayant accepté la proposition de l’établissement (admis)
- **acc_tot_f** => Dont effectif des candidates admises

- **acc_pp** => Effectif des admis en phase principale
- **acc_pc** => Effectif des admis en phase complémentaire

- (ne pas garder car peu utile) **acc_debutpp** => Dont effectif des admis ayant reçu leur proposition d’admission à l'ouverture de la procédure principale
- (ne pas garder car peu utile) **acc_datebac** => Dont effectif des admis ayant reçu leur proposition d’admission avant le baccalauréat
- (ne pas garder car peu utile) **acc_finpp** => Dont effectif des admis ayant reçu leur proposition d’admission avant la fin de la procédure principale

- **acc_internat** => Dont effectif des admis en internat

- **acc_brs** => Dont effectif des admis boursiers néo bacheliers

- **acc_neobac** => Effectif des admis néo bacheliers

- **acc_bg** => Effectif des admis néo bacheliers généraux

- **acc_bt** => Effectif des admis néo bacheliers technologiques

- **acc_bp** => Effectif des admis néo bacheliers professionnels

- **acc_at** => Effectif des autres candidats admis

- **acc_mention_nonrenseignee** => Dont effectif des admis néo bacheliers sans information sur la mention au bac
- **acc_sansmention** => Dont effectif des admis néo bacheliers sans mention au bac
- **acc_ab** => Dont effectif des admis néo bacheliers avec mention Assez Bien au bac
- **acc_b** => Dont effectif des admis néo bacheliers avec mention Bien au bac
- **acc_tb** => Dont effectif des admis néo bacheliers avec mention Très Bien au bac
- **acc_tbf** => Dont effectif des admis néo bacheliers avec mention Très Bien avec félicitations au bac
- **acc_bg_mention** => Effectif des admis néo bacheliers généraux ayant eu une mention au bac
- **acc_bt_mention** => Effectif des admis néo bacheliers technologiques ayant eu une mention au bac
- **acc_bp_mention** => Effectif des admis néo bacheliers professionnels ayant eu une mention au bac

- **acc_term** => Dont effectif des admis issus du même établissement (BTS/CPGE)
- **acc_term_f** => Dont effectif des admises issues du même établissement (BTS/CPGE)

- **acc_aca_orig** => Dont effectif des admis issus de la même académie
- **acc_aca_orig_idf** => Dont effectif des admis issus de la même académie (Paris/Créteil/Versailles réunies)

### Regroupements (ne pas garder ?)

- **lib_grp1** => Regroupement 1 effectué par les formations pour les classements
- **ran_grp1** => Rang du dernier appelé du groupe 1

- **lib_grp2** => Regroupement 2 effectué par les formations pour les classements
- **ran_grp2** => Rang du dernier appelé du groupe 2

- **lib_grp3** => Regroupement 3 effectué par les formations pour les classements
- **ran_grp3** => Rang du dernier appelé du groupe 3

## Problèmes

prop_tot = (prop_tot_bg + prop_tot_bt + prop_tot_bp + prop_tot_at) sauf pour `2972`

pourcentages inexacts
