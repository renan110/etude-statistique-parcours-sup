# Réponses aux questions

## Exercice 1 : Comprendre les données

### Q1. Analyse du fichier récupéré

1. Combien y-a t-il de lignes ?

   **13 396 lignes**

2. Que représente une ligne ?

   **Chaque ligne représente une formation proposée sur Parcoursup**

3. Combien y-a t-il de colonnes ?

   **118**

4. Quels sont les entêtes ?

   **Voir data/readable_header.txt** ou **data/usable_header.txt**

5. Quelle colonne identifie un établissement ? (numéro et nom)

   **n4**/**Code UAI de l'établissement**/**cod_uai**

6. Quelle colonne identifie une formation ? (numéro et nom)

   **n110**/**COD_AFF_FORM**/**cod_aff_form**

7. Combien de lignes font référence à notre département Informatique ?

   **1 seule car une ligne par formation**

8. Quelle colonne identifie un département ? (numéro et nom)

   **n5**/**Code départemental de l’établissement**/**dep**

9. Quelle structure de table pour les importer ?
   .

10. Quels problèmes identifiez vous ?

### Q2. Importer les données

1. Créer une table import permettant l’importation de ces données

2. S’assurer que les types de colonnes soient les plus restrictifs possibles (des int pour les colonnes contenant des entiers, des char(x) pour les données textuelles de taille x etc ...)

3. Remplir cette table avec les données récupérées

4. En s’appuyant sur la table import écrire les requêtes qui permettent de savoir
   1. Combien il y a de formations gérés par ParcourSup ?

      **13 996 (== nb lignes import)**

      ```sql
      SELECT COUNT(DISTINCT cod_aff_form) from import;
      ```

   2. Combien il y a d’établissements gérés par ParcourSup ?

      **3 869**

      ```sql
      SELECT COUNT(DISTINCT cod_uai) from import;
      ```

   3. Combien il y a de formations pour l’université de Lille ?

      **121**

      ```sql
      SELECT COUNT(cod_aff_form) from import WHERE g_ea_lib_vx LIKE '%Université de Lille%' AND g_ea_lib_vx NOT LIKE '%IUT%';
      ```

   4. Combien il y a de formations pour l’IUT-A ?

      **10**

      ```sql
      SELECT COUNT(cod_aff_form) from import WHERE g_ea_lib_vx= 'I.U.T. de Lille A';
      ```

   5. Citez 5 colonnes contenant des valeurs nulles
      - detail_forma (**n15**)
      - detail_forma2 (**n112**)
      - acc_term (**n69**)
      - acc_internat (**n53**)
      - lib_grp1 (**n101**)

      Requête de vérification:

      ```sql
      SELECT COUNT(*) from import WHERE <nom-colonne> = NULL;
      ```

## Exercice 2 : Ventiler les données

### Q1. Normalisation des données

1. Fournir le MCD correspondant à votre structuration.

2. Ecrire le script `parcourssup.sql` qui permet de réaliser toutes les actions d’importation et de création/remplissage des
différentes parcourssup.
On fera en sorte que ce script soit idempotent (on peut le lancer autant de fois que l’on veut, il donne toujours le même
résultat.)

### Q2. Une question de taille !

1. Quelle taille en octet fait le fichier récupéré ?

   **`12 744 468` octets**

2. Quelle taille en octet fait la table import ?

   **`16 064 512` octets**

   ```sql
   SELECT pg_relation_size('import');
   ```

3. Quelle taille en octet fait la somme des tables créées ?

   **`3 588 096` octets**

   ```sql
   SELECT pg_relation_size('formation') +  pg_relation_size('etablissement') + pg_relation_size('filiere')+ pg_relation_size('departement') + pg_relation_size('typefiliere') + pg_relation_size('typeprecisfiliere') + pg_relation_size('specialitefiliere');
   ```

4. Quelle taille en octet fait la somme des tailles des fichiers exportés correspondant à ces tables ?

   **`1 787 624` + `3 175` + `7 906` + `125` + `1 651` + `23 036` + `224 230` = `2 047 747` octets**

   ```sql
   \copy formation TO 'formation' CSV DELIMITER ';' ENCODING 'UTF8';
   \copy departement TO 'departement' CSV DELIMITER ';' ENCODING 'UTF8';
   \copy filiere TO 'filiere' CSV DELIMITER ';' ENCODING 'UTF8';
   \copy typefiliere TO 'typefiliere' CSV DELIMITER ';' ENCODING 'UTF8';
   \copy typeprecisfiliere TO 'typeprecisfiliere' CSV DELIMITER ';' ENCODING 'UTF8';
   \copy specialitefiliere TO 'specialitefiliere' CSV DELIMITER ';' ENCODING 'UTF8';
   \copy etablissement TO 'etablissement' CSV DELIMITER ';' ENCODING 'UTF8';
   ```

   ```bash
   wc -c formation departement filiere typefiliere typeprecisfiliere specialitefiliere etablissement 
   ```
